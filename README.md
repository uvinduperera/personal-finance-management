Automated Expense Management System for Personal Finances

Excited to share my latest project on personal finance management! As a DevOps Engineer with no financial background, I wanted to streamline and automate my expense tracking process. So, I developed an Expense Management System using iOS Numbers and leveraged my automation skills to create an iOS Shortcut for easy data entry.

This system goes beyond traditional expense tracking by incorporating a budget column for each expense category. It dynamically calculates the remaining budget as you enter expenses, enabling better decision-making on when to make purchases and when to hold back.

I believe in the power of open-source collaboration, so I'm thrilled to share the Number sheets and iOS Shortcut automation with everyone on GitHub. By making it open-source, I hope to empower individuals to take control of their personal finances and enhance their financial management skills.

Check out the Expense Management System on GitLab and feel free to customize it to suit your needs. Let's automate and manage our personal expenses together! 

#PersonalFinance #ExpenseManagement #Automation #OpenSource"